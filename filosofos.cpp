#include<stdio.h>
#include<stdlib.h>
#include<pthread.h>
#include<string.h>


#define atoa(x) #x
int comida = 100;//comida Global
int limEstomago = 10;//Limite del estomago de cada filosofo

pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;

struct filosofo{
	int nombre;
	int estomago;
	int cantComio = 0;
	struct tenedor * ten1;
	struct tenedor * ten2;

};

struct tenedor {
	int estado;
};

void * pensar(void * h1){
	
	struct filosofo *fil;
	fil = (struct filosofo*) h1;
	printf("%d %s \n", fil->nombre, "Esta pensando");
	fil->estomago--;
}

void * comer (void * h1){

	struct filosofo * fil;

	fil = (struct filosofo*) h1;
 	
	while(comida > 0){	
		if(fil->estomago < limEstomago){
			printf("%d %s \n", fil->nombre, " tiene hambre");
		
			if(fil->ten1->estado == 0 && fil->ten2->estado == 0){
		
				fil->ten1->estado = fil->ten2->estado = 1;
				printf("%d %s \n", fil->nombre, " agarro 2 tenedores");
				pthread_mutex_lock(&mutex);
				if(comida > 0){
					comida--;
				}else{
	
					pthread_mutex_unlock(&mutex);
					break;
				}		
				pthread_mutex_unlock(&mutex);	

				printf("%d %s \n", fil->nombre, "Esta comiendo");
				fil->ten1->estado = fil->ten2->estado = 0;
				fil->estomago++;
				fil->cantComio++;		

			}else{
		
				printf("%d %s \n", fil->nombre, " no puede comer");
	
			}
			
		}else{
			pthread_t thread;
			pthread_create(&thread, NULL, &pensar, (void *) fil);
			pthread_join(thread, NULL);
		}

		
	
	}


};

int main(){

	int n;
	scanf("%d", &n);
	pthread_t thread[n];
	struct filosofo filosofos[n];
	struct tenedor tenedores[n];
	printf("a");
	int iden[n];

	//inicializamos los tenedores
	for(int i = 0; i < n; i++){
		struct tenedor * ten = (struct tenedor *) malloc (sizeof(struct tenedor));
			
		ten->estado = 0;
		tenedores[i] = *ten;

	}
	//creamos los filosofos
	for(int i = 0; i < n; i++){
		
		struct filosofo * fil = (struct filosofo *) malloc (sizeof(struct filosofo));
		fil->nombre =  i;
		fil->estomago = 0;
		if(i != n-1){
			 
			fil->ten1 = &tenedores[i];
			fil->ten2 = &tenedores[i+1];
		}else{
			fil->ten1 = &tenedores[i];
			fil->ten2 = &tenedores[0];

		}
		filosofos[i] = *fil;

	}
	
	//creamos el hilo
	for(int i = 0; i < n; i++){
	
		pthread_create(&thread[i], NULL, &comer,(void *) &filosofos[i]);
	
	}
	//iniciamos el hilo
	
	for(int i = 0; i < n; i++){
	
		pthread_join(thread[i], NULL);

	}
	

	int sum = 0;
	for(int i = 0; i < n; i++){
		sum += filosofos[i].cantComio;	
		printf("El filosofo %d comio: %d \n", filosofos[i].nombre, filosofos[i].cantComio);		
	}

	printf("La suma de lo que comieron es: %d \n", sum);

	return 0;

}
